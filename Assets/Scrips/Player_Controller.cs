using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;

    public float upSpeed = 60f;
    public float RunSpeed = 10f;
    private float Points = 0f;

    private bool muerto = false;
    private bool EstaTocandoElSuelo = false;
    private bool Collider_Enemy = false;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (muerto == false) 
        {
            
            if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                setSlideAnimation();
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                Collider_Enemy = true;
            }

            else
            {
                if (Points >= 10)
                {
                    rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                    setIdleAnimation();
                }
                else
                {
                    rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                    setRunAnimation();
                    Collider_Enemy = false;
                }
                

            }
            
        }
        else
        {
            setJDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
        Debug.Log("Vel:" + RunSpeed);
        Debug.Log("Puntos:" + Points);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
        
        if(other.gameObject.layer == 8)
        {
            if (Collider_Enemy == true)
            {
                Destroy(other.gameObject);
                Points += 1;
                RunSpeed += 1.5f;
            }
            else
            {
                muerto = true;
            } 
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer == 7)
        {
            RunSpeed += 1.5f;
            Destroy(other.gameObject);
            Points += 1;
        }
    }

    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setJDeadAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
    private void setSlideAnimation()
    {
        _animator.SetInteger("Estado", 4);
    }
}
