using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Controller : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;

    public float RunSpeed;
    public float TimeT;
    private float Runtime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Runtime += Time.deltaTime;
        if (Runtime < TimeT)
        {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
        }
        if(Runtime > TimeT)
        {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
        }
        if (Runtime >= TimeT*2)
            Runtime = 0f;
    }
}
